package qa.bartszulc.proxy.selenium;

import com.codeborne.selenide.Configuration;
import org.junit.BeforeClass;

public class SeleniumTest {

    protected static final String JIRA_USERNAME = "admin";
    protected static final String JIRA_PASSWORD = "admin";
    protected static final String JIRA_ADDRESS = "https://bszulc.public.atlastunnel.com";

    protected static int PROXY_PORT = 9090;
    protected static String BASE_URL = String.format("http://localhost:%d", PROXY_PORT);

    @BeforeClass
    public static void setupClass() {
        Configuration.timeout = SeleniumConfiguration.timeout;
        Configuration.pollingInterval = SeleniumConfiguration.pollingInterval;
        Configuration.baseUrl = BASE_URL;
        Configuration.startMaximized = true;
    }
}
