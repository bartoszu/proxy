package qa.bartszulc.proxy.response;

import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;

public interface Response {

    ResponseDefinitionBuilder append(ResponseDefinitionBuilder responseDefinitionBuilder);

    default Response and(Response response) {
        return new ChainedResponse(append(response.definition()));
    }

    default ResponseDefinitionBuilder definition() {
        return append(ResponseDefinitionBuilder.responseDefinition());
    }

    class ChainedResponse implements Response {
        final ResponseDefinitionBuilder responseDefinitionBuilder;

        ChainedResponse(ResponseDefinitionBuilder responseDefinitionBuilder) {
            this.responseDefinitionBuilder = responseDefinitionBuilder;
        }

        @Override
        public ResponseDefinitionBuilder append(ResponseDefinitionBuilder responseDefinitionBuilder) {
            return responseDefinitionBuilder;
        }

        @Override
        public ResponseDefinitionBuilder definition() {
            return responseDefinitionBuilder;
        }
    }
}
