package qa.bartszulc.proxy.selenium.telemetry;

import com.atlassian.jira.bean.telemetry.BrowserMetrics;
import java.util.List;
import java.util.Optional;
import wiremock.com.google.common.collect.Lists;

public class Marker<T> {
    private final String key;
    private final Optional<Long> entry;
    private final Optional<Long> exit;
    private final T value;

    public Marker(final String key, T value, final Long entry, final Long exit) {
        this.key = key;
        this.value = value;
        this.entry = Optional.ofNullable(entry);
        this.exit = Optional.ofNullable(exit);
    }

    public static Optional<Marker<Long>> of(long earlierEvent,
                                            String earlierEventName,
                                            long laterEvent,
                                            String laterEventName,
                                            String alias) {
        if (earlierEvent != 0 && laterEvent != 0) {
            return Optional.of(new Marker<>(String.format("%s -> %s (%s)", earlierEventName, laterEventName, alias), (laterEvent - earlierEvent), earlierEvent, laterEvent));
        }
        return Optional.empty();
    }

    public static <T> Marker<T> of(String key, T value) {
        return new Marker<>(key, value, null, null);
    }

    public static List<Marker<?>> of(BrowserMetrics browserMetrics) {
        final Double apdex = Double.valueOf(browserMetrics.getApdex());
        final Long firstPaint = Long.valueOf(browserMetrics.getFirstPaint());
        final Long readyForUser = Long.valueOf(browserMetrics.getReadyForUser());
        final List<Marker<?>> markers = Lists.newArrayListWithCapacity(3);
        markers.add(new Marker<>("apdex", apdex, null, null));
        markers.add(new Marker<>("firstPaint", firstPaint, firstPaint, null));
        markers.add(new Marker<>("readyForUser", readyForUser, readyForUser, null));
        return markers;
    }

    public static int compare(Marker<?> marker1, Marker<?> marker2) {
        final Long entry1 = marker1.getEntry().orElse(null);
        final Long entry2 = marker2.getEntry().orElse(null);
        if (entry1 == null) {
            return 1;
        }
        if (entry2 == null) {
            return -1;
        }
        return Long.compare(entry1, entry2);
    }

    public String getKey() {
        return key;
    }

    public Optional<Long> getEntry() {
        return entry;
    }

    public Optional<Long> getExit() {
        return exit;
    }

    public T getValue() {
        return value;
    }
}