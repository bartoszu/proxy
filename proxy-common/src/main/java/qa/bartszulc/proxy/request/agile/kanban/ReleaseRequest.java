package qa.bartszulc.proxy.request.agile.kanban;

import com.github.tomakehurst.wiremock.client.MappingBuilder;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;
import qa.bartszulc.proxy.request.Request;
import qa.bartszulc.proxy.request.agile.scrum.BacklogDataRequest;

public class ReleaseRequest implements Request {
    @Override
    public MappingBuilder mapping() {
        return post(urlPathEqualTo("/rest/greenhopper/1.0/release"));
    }

    @Override
    public String getName() {
        return BacklogDataRequest.class.getSimpleName();
    }
}
