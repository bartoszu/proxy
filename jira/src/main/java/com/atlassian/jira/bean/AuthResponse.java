package com.atlassian.jira.bean;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthResponse {
    private final Session session;

    @JsonCreator
    public AuthResponse(@JsonProperty("session") Session session) {
        this.session = session;
    }

    public Session getSession() {
        return session;
    }

    public static class Session {
        private final String name;
        private final String value;

        @JsonCreator
        public Session(@JsonProperty("name") String name, @JsonProperty("value") String value) {
            this.name = name;
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public String getValue() {
            return value;
        }
    }
}
