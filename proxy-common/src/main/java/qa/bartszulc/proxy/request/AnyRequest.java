package qa.bartszulc.proxy.request;

import com.github.tomakehurst.wiremock.client.MappingBuilder;
import static com.github.tomakehurst.wiremock.client.WireMock.any;
import static com.github.tomakehurst.wiremock.client.WireMock.anyUrl;

public class AnyRequest implements Request {
    @Override
    public MappingBuilder mapping() {
        return any(anyUrl());
    }

    @Override
    public String getName() {
        return AnyRequest.class.getSimpleName();
    }
}
