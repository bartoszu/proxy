package qa.bartszulc.proxy.selenium.telemetry;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Navigation {

    private final long type;
    private final long redirectCount;

    @JsonCreator
    public Navigation(@JsonProperty("type") long type, @JsonProperty("redirectCount") long redirectCount) {
        this.type = type;
        this.redirectCount = redirectCount;
    }

    public long getType() {
        return type;
    }

    public long getRedirectCount() {
        return redirectCount;
    }
}
