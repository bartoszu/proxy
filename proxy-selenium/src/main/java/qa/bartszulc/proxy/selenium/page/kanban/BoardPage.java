package qa.bartszulc.proxy.selenium.page.kanban;

import com.codeborne.selenide.Condition;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import com.codeborne.selenide.SelenideElement;
import java.util.Optional;
import java.util.function.Supplier;
import qa.bartszulc.proxy.selenium.SeleniumConfiguration;
import qa.bartszulc.proxy.selenium.element.ErrorFlag;
import static qa.bartszulc.proxy.selenium.page.kanban.BoardPage.Elements.releaseAction;
import static qa.bartszulc.proxy.selenium.page.kanban.BoardPage.Elements.releaseButton;
import static qa.bartszulc.proxy.selenium.page.kanban.BoardPage.Elements.releaseDialog;

public class BoardPage {

    public BoardPage release() {
        clickReleaseButton();
        waitForReleaseDialog();
        submitReleaseDialogButton();
        return this;
    }

    private void clickReleaseButton() {
        releaseAction.get()
                .waitUntil(Condition.visible, SeleniumConfiguration.timeout, SeleniumConfiguration.pollingInterval)
                .click();
    }

    private void waitForReleaseDialog() {
        releaseDialog.get()
                .waitUntil(Condition.visible, SeleniumConfiguration.timeout, SeleniumConfiguration.pollingInterval)
                .click();
    }

    private void submitReleaseDialogButton() {
        releaseButton.get()
                .ifPresent(e -> e.waitUntil(Condition.visible, SeleniumConfiguration.timeout, SeleniumConfiguration.pollingInterval)
                        .click());
    }

    public String getError() {
        return new ErrorFlag().getMessage();
    }

    static class Elements {
        static Supplier<SelenideElement> releaseAction = () -> $("#ghx-release");
        static Supplier<SelenideElement> releaseDialog = () -> $("#ghx-dialog-release");
        static Supplier<Optional<SelenideElement>> releaseButton = () -> $$(".aui-button-primary")
                .stream()
                .filter(e -> e.text().equals("Release"))
                .findFirst();
    }
}
