package qa.bartszulc.proxy.request.telemetry;

import com.github.tomakehurst.wiremock.client.MappingBuilder;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;
import com.github.tomakehurst.wiremock.matching.MatchesJsonPathPattern;
import qa.bartszulc.proxy.request.Request;

public class BrowserMetricsRequest implements Request {

    private static final String FILTER_PATH = "$.[?(@.name == 'browser.metrics.navigation')].properties.key";
    private static final String EXTRACT_PATH = "$.[?(@.name == 'browser.metrics.navigation')].properties";
    private final String propertyValue;

    public BrowserMetricsRequest(String key) {
        this.propertyValue = String.format("[ \"%s\" ]", key);
    }

    @Override
    public MappingBuilder mapping() {
        return post(urlPathEqualTo("/rest/analytics/1.0/publish/bulk"))
                .withRequestBody(new MatchesJsonPathPattern(FILTER_PATH, equalTo(propertyValue)));
    }

    @Override
    public String jsonPath() {
        return EXTRACT_PATH;
    }

    @Override
    public String getName() {
        return BrowserMetricsRequest.class.getSimpleName();
    }
}
