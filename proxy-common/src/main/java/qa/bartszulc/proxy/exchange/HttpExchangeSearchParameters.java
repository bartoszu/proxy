package qa.bartszulc.proxy.exchange;


import java.util.Optional;
import qa.bartszulc.proxy.request.Request;

public final class HttpExchangeSearchParameters<T, V> {
    private final Request request;
    private final Optional<Class<T>> requestClass;
    private final Optional<Class<V>> responseClass;
    private final Optional<String> requestJsonPath;
    private final Optional<String> responseJsonPath;

    public HttpExchangeSearchParameters(final Request request,
                                        final Class<T> requestClass,
                                        final Class<V> responseClass,
                                        final String requestJsonPath,
                                        final String responseJsonPath) {
        this.request = request;
        this.requestClass = Optional.ofNullable(requestClass);
        this.responseClass = Optional.ofNullable(responseClass);
        this.requestJsonPath = requestJsonPath != null ? Optional.of(requestJsonPath) : Optional.ofNullable(request.jsonPath());
        this.responseJsonPath = Optional.ofNullable(responseJsonPath);
    }

    public static <T> HttpExchangeSearchParameters<Void, T> response(final Request request,
                                                                     final Class<T> responseClass) {
        return Builder.<Void, T>of(request)
                .withResponseClass(responseClass)
                .build();
    }

    public static <T> HttpExchangeSearchParameters<Void, T> response(final Request request,
                                                                     final Class<T> responseClass,
                                                                     final String responseJsonPath) {
        return Builder.<Void, T>of(request)
                .withResponseClass(responseClass)
                .withResponseJsonPath(responseJsonPath)
                .build();
    }

    public static <T> HttpExchangeSearchParameters<T, Void> request(final Request request,
                                                                    final Class<T> requestClass) {
        return Builder.<T, Void>of(request)
                .withRequestClass(requestClass)
                .build();
    }

    public static <T> HttpExchangeSearchParameters<T, Void> request(final Request request,
                                                                    final Class<T> requestClass,
                                                                    final String requestJsonPath) {
        return Builder.<T, Void>of(request)
                .withRequestClass(requestClass)
                .withRequestJsonPath(requestJsonPath)
                .build();
    }

    public static HttpExchangeSearchParameters<Void, Void> request(final Request request) {
        return Builder.<Void, Void>of(request).build();
    }

    public Request getRequest() {
        return request;
    }

    public Optional<Class<T>> getRequestClass() {
        return requestClass;
    }

    public Optional<Class<V>> getResponseClass() {
        return responseClass;
    }

    public Optional<String> getRequestJsonPath() {
        return requestJsonPath;
    }

    public Optional<String> getResponseJsonPath() {
        return responseJsonPath;
    }

    public static final class Builder<T, V> {
        private Request request = null;
        private Class<T> requestClass = null;
        private Class<V> responseClass = null;
        private String requestJsonPath = null;
        private String responseJsonPath = null;

        public static <T, V> Builder<T, V> of(Request request) {
            return new Builder<T, V>()
                    .withRequest(request);
        }

        public Builder<T, V> withRequest(Request request) {
            this.request = request;
            return this;
        }

        public Builder<T, V> withRequestClass(Class<T> requestClass) {
            this.requestClass = requestClass;
            return this;
        }

        public Builder<T, V> withRequestJsonPath(String requestJsonPath) {
            this.requestJsonPath = requestJsonPath;
            return this;
        }

        public Builder<T, V> withResponseClass(Class<V> responseClass) {
            this.responseClass = responseClass;
            return this;
        }

        public Builder<T, V> withResponseJsonPath(String responseJsonPath) {
            this.responseJsonPath = responseJsonPath;
            return this;
        }

        public HttpExchangeSearchParameters<T, V> build() {
            return new HttpExchangeSearchParameters<>(request,
                    requestClass, responseClass, requestJsonPath, responseJsonPath);
        }
    }
}
