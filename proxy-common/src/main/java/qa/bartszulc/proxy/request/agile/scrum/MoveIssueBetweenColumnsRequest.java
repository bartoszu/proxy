package qa.bartszulc.proxy.request.agile.scrum;

import com.github.tomakehurst.wiremock.client.MappingBuilder;
import static com.github.tomakehurst.wiremock.client.WireMock.put;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;
import qa.bartszulc.proxy.request.Request;

public class MoveIssueBetweenColumnsRequest implements Request {
    @Override
    public MappingBuilder mapping() {
        return put(urlPathEqualTo("/rest/greenhopper/1.0/xboard/transitionAndRank"));
    }

    @Override
    public String getName() {
        return MoveIssueBetweenColumnsRequest.class.getSimpleName();
    }
}
