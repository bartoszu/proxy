package qa.bartszulc.proxy.wiremock.extension.telemetry;

import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import com.github.tomakehurst.wiremock.extension.Parameters;
import com.github.tomakehurst.wiremock.http.HttpHeaders;
import com.github.tomakehurst.wiremock.http.ResponseDefinition;
import com.google.common.collect.Lists;
import static com.google.common.collect.Lists.newArrayList;

class ProxiedResponseDefinitionBuilder extends ResponseDefinitionBuilder {

    private HttpHeaders additionalProxyRequestHeaders;

    public static ProxiedResponseDefinitionBuilder like(ResponseDefinition responseDefinition) {
        ProxiedResponseDefinitionBuilder builder = new ProxiedResponseDefinitionBuilder();
        builder.status = responseDefinition.getStatus();
        builder.statusMessage = responseDefinition.getStatusMessage();
        builder.headers = responseDefinition.getHeaders() != null ?
                newArrayList(responseDefinition.getHeaders().all()) :
                Lists.newArrayList();
        builder.binaryBody = responseDefinition.getByteBodyIfBinary();
        builder.stringBody = responseDefinition.getBody();
        builder.base64Body = responseDefinition.getBase64Body();
        builder.bodyFileName = responseDefinition.getBodyFileName();
        builder.fixedDelayMilliseconds = responseDefinition.getFixedDelayMilliseconds();
        builder.delayDistribution = responseDefinition.getDelayDistribution();
        builder.chunkedDribbleDelay = responseDefinition.getChunkedDribbleDelay();
        builder.proxyBaseUrl = responseDefinition.getProxyBaseUrl();
        builder.fault = responseDefinition.getFault();
        builder.responseTransformerNames = responseDefinition.getTransformers();
        builder.transformerParameters = responseDefinition.getTransformerParameters() != null ?
                Parameters.from(responseDefinition.getTransformerParameters()) :
                Parameters.empty();
        builder.wasConfigured = responseDefinition.isFromConfiguredStub();
        builder.additionalProxyRequestHeaders = responseDefinition.getAdditionalProxyRequestHeaders();
        return builder;
    }

    @Override
    public ResponseDefinition build() {
        return super.build(additionalProxyRequestHeaders);
    }
}