package qa.bartszulc.proxy.selenium.fault.scrum;

import com.atlassian.jira.bean.CreateSprintResponse;
import com.codeborne.selenide.Selenide;
import com.github.tomakehurst.wiremock.http.Fault;
import java.util.List;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.Before;
import org.junit.Test;
import qa.bartszulc.proxy.exchange.HttpExchange;
import static qa.bartszulc.proxy.exchange.HttpExchangeSearchParameters.response;
import static qa.bartszulc.proxy.request.agile.AgileRequests.createSprint;
import static qa.bartszulc.proxy.request.agile.AgileRequests.deleteSprint;
import qa.bartszulc.proxy.response.FaultyResponse;
import static qa.bartszulc.proxy.response.StatusResponse.forbidden;
import static qa.bartszulc.proxy.response.StatusResponse.unauthorized;
import qa.bartszulc.proxy.selenium.fault.ProxyTest;
import qa.bartszulc.proxy.selenium.page.LoginPage;
import qa.bartszulc.proxy.selenium.page.Pages;
import qa.bartszulc.proxy.selenium.page.scrum.BacklogPage;

public class TestBacklogFaults extends ProxyTest {
    private static final String projectKey = "SCRUM001";
    private static final int boardId = 1;

    @Before
    public void setup() {
        Selenide.open("/login.jsp", LoginPage.class).login(JIRA_USERNAME, JIRA_PASSWORD);
    }

    @Test
    public void shouldDisplayErrorMessageOnCreateSprintForbidden() {
        // given
        proxy.always(createSprint(boardId), forbidden());
        // when
        final String error = Selenide.open(Pages.backlogPage(boardId, projectKey), BacklogPage.class)
                .createSprint()
                .getError();
        // then
        assertThat(error, equalTo("Forbidden"));
    }

    @Test
    public void shouldDisplayErrorMessageOnDeleteSprintForbidden() {
        // given
        proxy.always(deleteSprint(boardId), unauthorized());
        // when
        final String error = Selenide.open(Pages.backlogPage(boardId, projectKey), BacklogPage.class)
                .deleteSprint(2)
                .getError();
        // then
        assertThat(error, equalTo("Unauthorized"));
    }

    @Test
    public void shouldAllowCreatingSprintsOnceConnectionIsBack() {
        // given
        proxy.once(createSprint(boardId), FaultyResponse.of(Fault.MALFORMED_RESPONSE_CHUNK));
        final BacklogPage backlogPage = Selenide.open(Pages.backlogPage(boardId, projectKey), BacklogPage.class);
        int numberOfSprints = backlogPage.getSprintsCount();
        // when
        backlogPage
                .createSprint()
                .createSprint();
        // then
        assertThat(backlogPage.getSprintsCount(), equalTo(numberOfSprints + 1));
        final List<HttpExchange<Void, CreateSprintResponse>> exchanges = proxy.exchanges(
                response(createSprint(boardId), CreateSprintResponse.class));
        assertThat(exchanges.stream()
                .filter(exchange -> exchange.getResponse().getEntity().isPresent())
                .count(), equalTo(1L));
    }
}
