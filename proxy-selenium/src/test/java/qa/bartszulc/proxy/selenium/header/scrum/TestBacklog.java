package qa.bartszulc.proxy.selenium.header.scrum;

import static com.codeborne.selenide.Selenide.open;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.Test;
import qa.bartszulc.proxy.selenium.header.ProxyTest;
import qa.bartszulc.proxy.selenium.page.Pages;
import qa.bartszulc.proxy.selenium.page.scrum.BacklogPage;

public class TestBacklog extends ProxyTest {
    private static final String projectKey = "SCRUM001";
    private static final int boardId = 1;

    @Test
    public void shouldCreateSprint() {
        // given
        BacklogPage backlogPage = open(Pages.backlogPage(boardId, projectKey), BacklogPage.class);
        int numberOfSprints = backlogPage.getSprintsCount();
        // when
        backlogPage.createSprint();
        // then
        assertThat(backlogPage.getSprintsCount(), equalTo(numberOfSprints + 1));
    }
}
