package qa.bartszulc.proxy.selenium.page;

public final class Pages {

    public static String backlogPage(int boardId, String projectKey) {
        return String.format("/secure/RapidBoard.jspa?rapidView=%s&projectKey=%s&view=planning.nodetail&issueLimit=100", boardId, projectKey);
    }

    public static String boardPage(int boardId, String projectKey) {
        return String.format("/secure/RapidBoard.jspa?rapidView=%s&projectKey=%s", boardId, projectKey);
    }
}
