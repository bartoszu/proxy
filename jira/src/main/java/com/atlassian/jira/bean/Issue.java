package com.atlassian.jira.bean;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Issue {

    private final int id;
    private final String key;
    private final boolean hidden;
    private final String typeId;
    private final String summary;
    private final String priorityId;
    private final boolean done;
    private final boolean hasCustomUserAvatar;
    private final boolean estimateStatisticRequired;
    private final String statusId;
    private final List<FixVersion> fixVersions;
    private final int projectId;
    private final EstimatedStatistic estimatedStatistic;

    @JsonIgnoreProperties(ignoreUnknown = true)
    public Issue(@JsonProperty("id") int id,
                 @JsonProperty("key") String key,
                 @JsonProperty("hidden") boolean hidden,
                 @JsonProperty("typeId") String typeId,
                 @JsonProperty("summary") String summary,
                 @JsonProperty("priorityId") String priorityId,
                 @JsonProperty("done") boolean done,
                 @JsonProperty("hasCustomUserAvatar") boolean hasCustomUserAvatar,
                 @JsonProperty("estimateStatisticRequired") boolean estimateStatisticRequired,
                 @JsonProperty("statusId") String statusId,
                 @JsonProperty("fixVersions") List<FixVersion> fixVersions,
                 @JsonProperty("projectId") int projectId,
                 @JsonProperty("estimatedStatistic") EstimatedStatistic estimatedStatistic) {
        this.id = id;
        this.key = key;
        this.hidden = hidden;
        this.typeId = typeId;
        this.summary = summary;
        this.priorityId = priorityId;
        this.done = done;
        this.hasCustomUserAvatar = hasCustomUserAvatar;
        this.estimateStatisticRequired = estimateStatisticRequired;
        this.statusId = statusId;
        this.fixVersions = fixVersions;
        this.projectId = projectId;
        this.estimatedStatistic = estimatedStatistic;
    }

    public int getId() {
        return id;
    }

    public String getKey() {
        return key;
    }

    public boolean isHidden() {
        return hidden;
    }

    public String getTypeId() {
        return typeId;
    }

    public String getSummary() {
        return summary;
    }

    public String getPriorityId() {
        return priorityId;
    }

    public boolean isDone() {
        return done;
    }

    public boolean isHasCustomUserAvatar() {
        return hasCustomUserAvatar;
    }

    public boolean isEstimateStatisticRequired() {
        return estimateStatisticRequired;
    }

    public String getStatusId() {
        return statusId;
    }

    public List<FixVersion> getFixVersions() {
        return fixVersions;
    }

    public int getProjectId() {
        return projectId;
    }

    public EstimatedStatistic getEstimatedStatistic() {
        return estimatedStatistic;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class StatFieldValue {
        private final String value;

        @JsonCreator
        public StatFieldValue(@JsonProperty("value") @JsonInclude(JsonInclude.Include.NON_NULL) String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class EstimatedStatistic {
        private final String statFieldId;
        private final StatFieldValue statFieldValue;

        @JsonCreator
        public EstimatedStatistic(@JsonProperty("statFieldId") String statFieldId,
                                  @JsonProperty("statFieldValue") StatFieldValue statFieldValue) {
            this.statFieldId = statFieldId;
            this.statFieldValue = statFieldValue;
        }

        public String getStatFieldId() {
            return statFieldId;
        }

        public StatFieldValue getStatFieldValue() {
            return statFieldValue;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class FixVersion {
        private final String versionId;

        @JsonCreator
        public FixVersion(@JsonProperty("versionId") String versionId) {
            this.versionId = versionId;
        }

        public String getVersionId() {
            return versionId;
        }
    }

}
