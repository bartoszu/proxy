package qa.bartszulc.proxy.selenium;

public interface SeleniumConfiguration {
    int timeout = 20000;
    int pollingInterval = 100;
}
