package qa.bartszulc.proxy.explore;


import com.github.tomakehurst.wiremock.extension.Extension;
import com.google.common.collect.ImmutableMap;
import static qa.bartszulc.proxy.Constants.FIXTURES;
import qa.bartszulc.proxy.Proxy;
import static qa.bartszulc.proxy.request.agile.AgileRequests.boardData;
import static qa.bartszulc.proxy.request.agile.AgileRequests.createSprint;
import static qa.bartszulc.proxy.request.agile.AgileRequests.deleteSprint;
import static qa.bartszulc.proxy.request.agile.AgileRequests.issueRank;
import static qa.bartszulc.proxy.request.agile.AgileRequests.moveIssueBetweenColumns;
import qa.bartszulc.proxy.request.agile.kanban.MoveIssueBetweenColumnsRequest;
import qa.bartszulc.proxy.request.agile.kanban.ReleaseRequest;
import static qa.bartszulc.proxy.response.DeferredResponse.deferred;
import static qa.bartszulc.proxy.response.DelayedResponse.delayed;
import static qa.bartszulc.proxy.response.FaultyResponse.connectionReset;
import static qa.bartszulc.proxy.response.FaultyResponse.malformed;
import qa.bartszulc.proxy.response.FileResponse;
import static qa.bartszulc.proxy.response.StatusResponse.forbidden;
import static qa.bartszulc.proxy.response.StatusResponse.unauthorized;
import qa.bartszulc.proxy.response.TransformedResponse;
import qa.bartszulc.proxy.wiremock.WireMockProxy;
import qa.bartszulc.proxy.wiremock.extension.GeneratedIssuesResponseTransformer;
import static qa.bartszulc.proxy.wiremock.extension.GeneratedIssuesResponseTransformer.NUM_ISSUES_KEY;


public class WiremockExplorer {

    public static void main(String[] args) {
        final Extension extension = new GeneratedIssuesResponseTransformer();
        Proxy proxy = new WireMockProxy(9090, FIXTURES, extension);
        proxy.from("https://bszulc.public.atlastunnel.com");

        // examples
        proxy.always(boardData(), FileResponse.of("board.json").and(delayed(500)));
        proxy.always(moveIssueBetweenColumns(), connectionReset());

        int rapidBoardId = 1;
        proxy.once(createSprint(rapidBoardId), unauthorized());
        proxy.always(deleteSprint(rapidBoardId), forbidden());
        proxy.always(issueRank(rapidBoardId), malformed());
        proxy.always(boardData(), FileResponse.of("board.template.json")
                .and(TransformedResponse.of(extension, ImmutableMap.of(NUM_ISSUES_KEY, 100))));

        // exercise
        // TODO: forbid releasing issues
        // TODO: forbid transitioning issues due to malformed response
        // TODO: move issues between columns while at the same time change status of the issue back to the initial with context menu
    }
}
