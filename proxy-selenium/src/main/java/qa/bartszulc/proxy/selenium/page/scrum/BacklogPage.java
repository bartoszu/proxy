package qa.bartszulc.proxy.selenium.page.scrum;


import com.codeborne.selenide.Condition;
import static com.codeborne.selenide.Condition.appears;
import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Condition.not;
import static com.codeborne.selenide.Condition.visible;
import com.codeborne.selenide.SelenideElement;
import qa.bartszulc.proxy.selenium.SeleniumConfiguration;
import qa.bartszulc.proxy.selenium.element.ErrorFlag;
import static qa.bartszulc.proxy.selenium.page.scrum.Elements.actionsMenu;
import static qa.bartszulc.proxy.selenium.page.scrum.Elements.addSprintAction;
import static qa.bartszulc.proxy.selenium.page.scrum.Elements.addSprintButton;
import static qa.bartszulc.proxy.selenium.page.scrum.Elements.backlogHeader;
import static qa.bartszulc.proxy.selenium.page.scrum.Elements.confirmDeleteDialog;
import static qa.bartszulc.proxy.selenium.page.scrum.Elements.deleteAction;
import static qa.bartszulc.proxy.selenium.page.scrum.Elements.backlogIssues;
import static qa.bartszulc.proxy.selenium.page.scrum.Elements.sprints;

public class BacklogPage {

    public BacklogPage createSprint() {
        if (!addSprintButton.get().isDisplayed()) {
            waitUntilBacklog();
            waitUntilCreateSprintDialog();
        }
        addSprintButton.get()
                .waitUntil(visible, SeleniumConfiguration.timeout, SeleniumConfiguration.pollingInterval)
                .click();
        return this;
    }

    public BacklogPage deleteSprint(int sprintId) {
        waitUntilBacklog();
        actionsMenu.apply(sprintId)
                .click();
        deleteAction.get()
                .click();
        confirmDeleteDialog.get()
                .waitUntil(appears, SeleniumConfiguration.timeout, SeleniumConfiguration.pollingInterval)
                .find(".aui-button")
                .click();
        return this;
    }

    public String getError() {
        return new ErrorFlag().getMessage();
    }

    public int getSprintsCount() {
        waitUntilBacklog();
        waitUntilAddSprintButtonDisabled();
        return sprints.get().size();
    }

    public BacklogPage waitForIssues() {
        backlogIssues.get().
                waitUntil(Condition.appears, SeleniumConfiguration.timeout, SeleniumConfiguration.pollingInterval);
        return this;
    }

    private void waitUntilBacklog() {
        backlogHeader.get()
                .waitUntil(visible, SeleniumConfiguration.timeout, SeleniumConfiguration.pollingInterval);
    }

    private SelenideElement createSprintButton() {
        return addSprintAction.get()
                .waitUntil(visible, SeleniumConfiguration.timeout, SeleniumConfiguration.pollingInterval);
    }

    private void waitUntilCreateSprintButtonDisabled() {
        createSprintButton()
                .waitUntil(not(attribute("aria-disabled", "true")), SeleniumConfiguration.timeout, SeleniumConfiguration.pollingInterval);
    }

    private void waitUntilCreateSprintDialog() {
        createSprintButton()
                .click();
        waitUntilCreateSprintButtonDisabled();
    }

    private void waitUntilAddSprintButtonDisabled() {
        addSprintButton.get()
                .waitUntil(not(visible), SeleniumConfiguration.timeout, SeleniumConfiguration.pollingInterval);
    }
}
