package qa.bartszulc.proxy.selenium.element;

import com.codeborne.selenide.Condition;
import static com.codeborne.selenide.Selenide.$;
import qa.bartszulc.proxy.selenium.SeleniumConfiguration;

public class ErrorFlag {

    public String getMessage() {
        return $(".aui-message-error > p", 1)
                .waitUntil(Condition.appears, SeleniumConfiguration.timeout, SeleniumConfiguration.pollingInterval)
                .getText();
    }
}
