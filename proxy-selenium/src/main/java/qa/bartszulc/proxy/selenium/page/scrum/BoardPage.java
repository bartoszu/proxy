package qa.bartszulc.proxy.selenium.page.scrum;

import com.codeborne.selenide.Condition;
import qa.bartszulc.proxy.selenium.SeleniumConfiguration;
import qa.bartszulc.proxy.selenium.element.ErrorFlag;
import static qa.bartszulc.proxy.selenium.page.scrum.Elements.boardIssues;

public class BoardPage {

    public BoardPage waitForIssues() {
        boardIssues.get()
                .waitUntil(Condition.appears, SeleniumConfiguration.timeout, SeleniumConfiguration.pollingInterval);
        return this;
    }

    public String getError() {
        return new ErrorFlag().getMessage();
    }
}
