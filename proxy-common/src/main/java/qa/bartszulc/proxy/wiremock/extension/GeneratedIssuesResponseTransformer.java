package qa.bartszulc.proxy.wiremock.extension;

import com.atlassian.jira.bean.Issue;
import com.atlassian.jira.bean.random.RandomIssue;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.common.FileSource;
import com.github.tomakehurst.wiremock.extension.Parameters;
import com.github.tomakehurst.wiremock.extension.ResponseTransformer;
import com.github.tomakehurst.wiremock.http.Request;
import com.github.tomakehurst.wiremock.http.Response;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GeneratedIssuesResponseTransformer extends ResponseTransformer {
    public static final String NUM_ISSUES_KEY = "issues";
    public static final String ISSUES_PROJECT_KEY = "projectKey";
    private static final int DEFAULT_NUM_ISSUES = 100;
    private static final String DEFAULT_ISSUES_PROJECT_KEY = "SCRUM001";
    private static final Logger LOG = LoggerFactory.getLogger(GeneratedIssuesResponseTransformer.class);
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    @Override
    public Response transform(Request request, Response response, FileSource files, Parameters parameters) {
        final Response.Builder builder = Response.Builder.like(response);
        return builder.body(response(response.getBodyAsString(), parameters)).build();
    }

    private String response(String original, Parameters parameters) {
        final List<Issue> issues = getIssues(getNumIssues(parameters), getIssuesProjectKey(parameters));
        final int[] issueIds = issues.stream().mapToInt(Issue::getId).toArray();
        final String issuesAsString = dump(issues);
        final String issueIdsAsString = dump(issueIds);
        return original
                .replace("${issues}", issuesAsString)
                .replace("${issueIds}", issueIdsAsString);
    }

    private int getNumIssues(Parameters parameters) {
        return parameters == null ? DEFAULT_NUM_ISSUES : parameters.getInt(NUM_ISSUES_KEY, DEFAULT_NUM_ISSUES);
    }

    private String getIssuesProjectKey(Parameters parameters) {
        return parameters == null ? DEFAULT_ISSUES_PROJECT_KEY : parameters.getString(ISSUES_PROJECT_KEY, DEFAULT_ISSUES_PROJECT_KEY);
    }

    @Override
    public String getName() {
        return GeneratedIssuesResponseTransformer.class.getName();
    }

    @Override
    public boolean applyGlobally() {
        return false;
    }

    private List<Issue> getIssues(int numOfIssues, String projectKey) {
        final RandomIssue generator = new RandomIssue();
        return IntStream.range(0, numOfIssues)
                .mapToObj(i -> generator.next(projectKey))
                .collect(Collectors.toList());

    }

    private <T> String dump(T object) {
        try {
            return OBJECT_MAPPER.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            LOG.warn("Couldn't generate issues", e);
            return "[]";
        }
    }
}
