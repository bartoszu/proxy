# requirements

- [mvnvm](http://mvnvm.org/)
- [intellij](https://www.jetbrains.com/idea/download/)
- [jira](https://pl.atlassian.com/software/jira/download)

# setup

- configure jira to work with a proxy, edit conf/server.xml, and start jira
- create two projects, SCRUM001 (Scrum project) and KANBAN001 (Kanban project)

# exploratory testing

## walk through 

- check qa.bartszulc.proxy.explore.WiremockExplorer
- explore jira
    - open scrum project
        - load active sprints with a delay
        - move issues between columns while connection reset
        - create sprint while unauthorized
        - delete sprint while forbidden
        - rank issue in a backlog while response malformed
        - dynamically generate issues displayed on a board
    
# on your own

- explore jira
    - open kanban project
        - release issues while unauthorized
        - move issues between columns while response malformed
    - open scrum project
        - move issues between columns under slow network and try changing status with a context menu while the transition is in progress
 
# automated testing

## fault conditions

### walk through

- analyse tests in qa.bartszulc.proxy.selenium.fault.scrum.TestBacklogFaults
    - creating sprint forbidden
    - delete sprint forbidden
    - create sprint recovers from a fault
    
### on your own

- complete tests qa.bartszulc.proxy.selenium.fault.kanban.TestBoardFaults
    - releasing issues forbidden
    
## authorisation

### walk through

- analyse tests in qa.bartszulc.proxy.selenium.header.scrum.TestBacklog
    - pass headers to authorise
    
### on your own

- refactor tests in qa.bartszulc.proxy.selenium.fault.kanban.TestBoardFaults
    - don't login with selenium, instead pass session header via proxy
    
## performance

### walk through

- analyse tests in qa.bartszulc.proxy.selenium.performance.scrum.TestBoard
    - performance indicators change with the number of issues displayed on a board
   
### on your own

- complete test in qa.bartszulc.proxy.selenium.performance.scrum.TestBacklog
    - see how performance of backlog changes with the number of issues displayed
