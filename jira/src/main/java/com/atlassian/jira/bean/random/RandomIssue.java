package com.atlassian.jira.bean.random;

import com.atlassian.jira.bean.Issue;
import java.util.Collections;

public class RandomIssue {

    private int id = 10000;
    private int counter = 1;

    public Issue next(String projectKey) {
        return next(projectKey, 10000);
    }

    public Issue next(String projectKey, int projectId) {
        final Issue boardIssue = new Issue(id,
                String.format("%s-%d", projectKey, id),
                false,
                "10002",
                String.format("test issue %d", counter),
                "3",
                false,
                false,
                false,
                "10000",
                Collections.emptyList(),
                projectId,
                new Issue.EstimatedStatistic("customfield_10006", new Issue.StatFieldValue(null)));
        counter++;
        id++;
        return boardIssue;
    }
}
