package qa.bartszulc.proxy.selenium.header;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import qa.bartszulc.proxy.selenium.SeleniumTest;
import qa.bartszulc.proxy.selenium.WireMockProxySupport;
import qa.bartszulc.proxy.wiremock.WireMockProxy;

public class ProxyTest extends SeleniumTest {

    protected static WireMockProxy proxy;

    @BeforeClass
    public static void setupClass() {
        proxy = WireMockProxySupport.tryAuthProxy(PROXY_PORT, JIRA_ADDRESS, JIRA_USERNAME, JIRA_PASSWORD);
        SeleniumTest.setupClass();
    }

    @AfterClass
    public static void teardownClass() {
        proxy.stop();
    }

    @Before
    public void setup() {
        proxy.reset();
    }
}