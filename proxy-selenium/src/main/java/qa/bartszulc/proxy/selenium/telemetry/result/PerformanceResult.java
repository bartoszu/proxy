package qa.bartszulc.proxy.selenium.telemetry.result;

import com.atlassian.jira.bean.telemetry.BrowserMetrics;
import com.google.common.collect.ImmutableList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.annotation.Nullable;
import qa.bartszulc.proxy.exchange.HttpExchange;
import qa.bartszulc.proxy.selenium.telemetry.Marker;
import wiremock.com.google.common.collect.Lists;

public class PerformanceResult {
    private final long issues;
    private final List<Marker<?>> markers;

    public PerformanceResult(final long issues,
                             final List<Marker<?>> markers) {
        this.issues = issues;
        this.markers = ImmutableList.copyOf(markers);
    }

    public static <T, V> PerformanceResult of(final long issues,
                                              final Long navigationStarted,
                                              final List<Optional<Marker<Long>>> standardMarkers,
                                              @Nullable final HttpExchange<BrowserMetrics, Void> browserMetrics,
                                              final List<HttpExchange<T, V>> exchanges,
                                              final List<Marker<?>> additionalMarkers) {
        final List<Marker<?>> exchangeMarkers = exchanges.stream()
                .map(exchange -> exchange.getRequestReceived()
                        .flatMap(requestReceived -> exchange.getResponseSent()
                                .map(responseSent ->
                                        new Marker<>(exchange.getOrigin().getName(),
                                                responseSent - requestReceived,
                                                requestReceived - navigationStarted,
                                                responseSent - navigationStarted)))
                        .orElse(null))
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        final List<Marker<?>> browserMetricsMarkers = Optional.ofNullable(browserMetrics)
                .map(metrics -> metrics.getRequest().getEntity()
                        .map(Marker::of)
                        .orElse(ImmutableList.of()))
                .orElse(ImmutableList.of());

        final List<Marker<?>> timingMarkers = standardMarkers.stream()
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());

        final List<Marker<?>> markers = Lists.newArrayList();
        markers.addAll(timingMarkers);
        markers.addAll(browserMetricsMarkers);
        markers.addAll(exchangeMarkers);
        markers.addAll(additionalMarkers);

        return new PerformanceResult(issues, markers);
    }

    public long getIssues() {
        return issues;
    }

    public List<Marker<?>> getMarkers() {
        return markers;
    }
}
