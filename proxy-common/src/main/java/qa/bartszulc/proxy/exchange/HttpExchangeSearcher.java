package qa.bartszulc.proxy.exchange;

import com.github.tomakehurst.wiremock.matching.RequestPattern;
import com.github.tomakehurst.wiremock.stubbing.ServeEvent;
import com.google.common.collect.ImmutableList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import static java.util.concurrent.TimeUnit.SECONDS;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import static org.awaitility.Awaitility.await;

public class HttpExchangeSearcher<T, V> {

    private final Supplier<List<ServeEvent>> eventsProvider;
    private final HttpExchangeSearchParameters<T, V> parameters;
    private final RequestPattern requestPattern;

    public HttpExchangeSearcher(final Supplier<List<ServeEvent>> eventsProvider,
                                final HttpExchangeSearchParameters<T, V> parameters) {
        this.eventsProvider = eventsProvider;
        this.parameters = parameters;
        this.requestPattern = parameters.getRequest()
                .mapping()
                .build()
                .getRequest();
    }

    private List<ServeEvent> events() {
        return ImmutableList.copyOf(eventsProvider.get());
    }

    private List<ServeEvent> lookup() {
        return events().stream()
                .filter(event -> requestPattern.match(event.getRequest()).isExactMatch())
                .collect(Collectors.toList());
    }

    private List<HttpExchange<T, V>> parse() {
        return lookup().stream()
                .map(this::parse)
                .collect(Collectors.toList());
    }

    private HttpExchange<T, V> parse(final ServeEvent serveEvent) {
        return HttpExchange.of(serveEvent, parameters);
    }

    public List<HttpExchange<T, V>> exchanges() {
        return awaitExchanges(30, SECONDS);
    }

    public List<HttpExchange<T, V>> awaitExchanges(final long timeout, final TimeUnit unit) {
        await().atMost(timeout, unit).until(() -> !lookup().isEmpty());
        return parse();
    }
}
