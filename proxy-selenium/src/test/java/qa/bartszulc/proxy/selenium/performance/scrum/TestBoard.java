package qa.bartszulc.proxy.selenium.performance.scrum;

import static com.codeborne.selenide.Selenide.open;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import static qa.bartszulc.proxy.exchange.HttpExchangeSearchParameters.request;
import qa.bartszulc.proxy.request.agile.AgileRequests;
import static qa.bartszulc.proxy.request.agile.AgileRequests.boardData;
import qa.bartszulc.proxy.response.FileResponse;
import qa.bartszulc.proxy.response.TransformedResponse;
import qa.bartszulc.proxy.selenium.page.Pages;
import qa.bartszulc.proxy.selenium.page.scrum.BoardPage;
import qa.bartszulc.proxy.selenium.performance.PerformanceTest;
import static qa.bartszulc.proxy.wiremock.extension.GeneratedIssuesResponseTransformer.ISSUES_PROJECT_KEY;
import static qa.bartszulc.proxy.wiremock.extension.GeneratedIssuesResponseTransformer.NUM_ISSUES_KEY;

@RunWith(Parameterized.class)
public class TestBoard extends PerformanceTest {
    private static final String PROJECT_KEY = "SCRUM001";
    private static final int BOARD_ID = 1;

    public TestBoard(int issues) {
        super(issues);
    }

    @Test
    public void openBoard() {
        // given
        proxy.once(boardData(), FileResponse.of("board.template.json").and(
                TransformedResponse.of(ISSUES_EXT, ImmutableMap.of(
                        NUM_ISSUES_KEY, issues,
                        ISSUES_PROJECT_KEY, PROJECT_KEY
                ))));
        // when
        open(Pages.boardPage(BOARD_ID, PROJECT_KEY), BoardPage.class).waitForIssues();
        // then
        proxy.exchange(request(boardData()))
                .ifPresent(exchange -> recordPerformance(exchange, AgileRequests.Metrics.board()));
    }
}
