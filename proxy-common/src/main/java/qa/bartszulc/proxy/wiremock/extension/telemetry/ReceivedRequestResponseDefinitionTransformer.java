package qa.bartszulc.proxy.wiremock.extension.telemetry;

import com.github.tomakehurst.wiremock.common.FileSource;
import com.github.tomakehurst.wiremock.extension.Parameters;
import com.github.tomakehurst.wiremock.extension.ResponseDefinitionTransformer;
import com.github.tomakehurst.wiremock.http.HttpHeader;
import com.github.tomakehurst.wiremock.http.HttpHeaders;
import com.github.tomakehurst.wiremock.http.Request;
import com.github.tomakehurst.wiremock.http.ResponseDefinition;
import java.util.Optional;

public class ReceivedRequestResponseDefinitionTransformer extends ResponseDefinitionTransformer {
    public static final String RECEIVED_REQUEST_HEADER = "X-Proxy-ReceivedRequest";

    @Override
    public ResponseDefinition transform(final Request request,
                                        final ResponseDefinition responseDefinition,
                                        final FileSource files,
                                        final Parameters parameters) {
        final ProxiedResponseDefinitionBuilder builder = ProxiedResponseDefinitionBuilder.like(responseDefinition);
        final HttpHeaders headers = Optional.ofNullable(responseDefinition.getHeaders()).orElse(HttpHeaders.noHeaders());
        builder.withHeaders(headers.plus(new HttpHeader(RECEIVED_REQUEST_HEADER, String.valueOf(System.currentTimeMillis()))));
        return builder.build();
    }

    @Override
    public String getName() {
        return ReceivedRequestResponseDefinitionTransformer.class.getName();
    }
}
