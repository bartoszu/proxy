package qa.bartszulc.proxy.selenium.page;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;
import com.codeborne.selenide.SelenideElement;
import java.util.function.Supplier;
import org.openqa.selenium.By;

public class LoginPage {
    public DashboardPage login(String username, String password) {
        Elements.username.get()
                .setValue(username);
        Elements.password.get()
                .setValue(password).pressEnter();
        return page(DashboardPage.class);
    }

    static class Elements {
        static Supplier<SelenideElement> username = () -> $(By.name("os_username"));
        static Supplier<SelenideElement> password = () -> $(By.name("os_password"));
    }
}
