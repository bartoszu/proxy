package qa.bartszulc.proxy.selenium.page.scrum;

import com.codeborne.selenide.ElementsCollection;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import com.codeborne.selenide.SelenideElement;
import java.util.function.IntFunction;
import java.util.function.Supplier;

class Elements {
    static IntFunction<SelenideElement> actionsMenu = sprintId -> $(String.format(".js-sprint-actions-trigger[data-sprint-id=\"%s\"]", sprintId));
    static Supplier<SelenideElement> deleteAction = () -> $("#gh-sprint-delete");
    static Supplier<SelenideElement> confirmDeleteDialog = () -> $("#ghx-remove-planned-sprint-confirm-dialog");
    static Supplier<ElementsCollection> sprints = () -> $$("span[data-fieldName='sprintName']");
    static Supplier<SelenideElement> backlogIssues = () -> $(".ghx-issues");
    static Supplier<SelenideElement> boardIssues = () -> $(".ghx-issue");
    static Supplier<SelenideElement> backlogHeader = () -> $(".ghx-backlog-header");
    static Supplier<SelenideElement> addSprintAction = () -> $(".js-add-sprint");
    static Supplier<SelenideElement> addSprintButton = () -> $(".ghx-add-sprint-button");
}