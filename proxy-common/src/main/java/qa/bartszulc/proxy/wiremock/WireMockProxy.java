package qa.bartszulc.proxy.wiremock;

import com.github.tomakehurst.wiremock.WireMockServer;
import static com.github.tomakehurst.wiremock.client.WireMock.any;
import static com.github.tomakehurst.wiremock.client.WireMock.anyUrl;
import com.github.tomakehurst.wiremock.common.ConsoleNotifier;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import com.github.tomakehurst.wiremock.extension.Extension;
import com.github.tomakehurst.wiremock.stubbing.Scenario;
import com.google.common.collect.ImmutableList;
import java.util.List;
import java.util.Optional;
import static java.util.UUID.randomUUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import qa.bartszulc.proxy.Proxy;
import qa.bartszulc.proxy.exchange.HttpExchange;
import qa.bartszulc.proxy.exchange.HttpExchangeSearchParameters;
import qa.bartszulc.proxy.exchange.HttpExchangeSearcher;
import qa.bartszulc.proxy.request.Request;
import qa.bartszulc.proxy.response.ProxyResponse;
import qa.bartszulc.proxy.response.Response;
import qa.bartszulc.proxy.wiremock.extension.DeferredResponseDefinitionTransformer;
import qa.bartszulc.proxy.wiremock.extension.telemetry.ReceivedRequestResponseDefinitionTransformer;
import qa.bartszulc.proxy.wiremock.extension.telemetry.SentResponseResponseTransformer;


public class WireMockProxy implements Proxy {
    private static final Logger LOG = LoggerFactory.getLogger(WireMockProxy.class);
    private static final WireMockConfiguration DEFAULT_WM_OPTIONS = options()
            .notifier(new ConsoleNotifier(true));

    private final WireMockServer wm;
    private String fromBaseUrl;
    private List<Header> headers;

    public WireMockProxy(int port, String filesDirectory, Extension... extensions) {
        this(DEFAULT_WM_OPTIONS.port(port)
                .withRootDirectory(filesDirectory)
                .extensions(new ReceivedRequestResponseDefinitionTransformer())
                .extensions(new DeferredResponseDefinitionTransformer())
                .extensions(extensions)
                .extensions(new SentResponseResponseTransformer()));
    }

    public WireMockProxy(int port) {
        this(DEFAULT_WM_OPTIONS
                .extensions(new ReceivedRequestResponseDefinitionTransformer())
                .extensions(new DeferredResponseDefinitionTransformer())
                .extensions(new SentResponseResponseTransformer())
                .port(port));
    }

    private WireMockProxy(final WireMockConfiguration options) {
        this.wm = new WireMockServer(options);
    }

    @Override
    public WireMockProxy from(String fromBaseUrl, Header... headers) {
        this.fromBaseUrl = fromBaseUrl;
        this.headers = ImmutableList.copyOf(headers);
        return proxy();
    }

    @Override
    public WireMockProxy always(Request request, Response response) {
        wm.stubFor(request.mapping()
                .willReturn(response.definition())
                .atPriority(1));
        return this;
    }

    @Override
    public WireMockProxy stub(Request request, Response... response) {
        final String scenario = randomUUID().toString();
        for (int i = 0; i < response.length; i++) {
            wm.stubFor(request.mapping()
                    .inScenario(scenario)
                    .whenScenarioStateIs(step(scenario, i))
                    .willReturn(response[i].definition())
                    .willSetStateTo(step(scenario, i + 1))
            );
        }
        return this;
    }

    @Override
    public WireMockProxy once(Request request, Response response) {
        stub(request, response);
        return this;
    }

    @Override
    public WireMockProxy reset() {
        wm.resetAll();
        proxy();
        return this;
    }

    @Override
    public void stop() {
        wm.stop();
    }

    @Override
    public int totalRequests() {
        return wm.getAllServeEvents().size();
    }

    @Override
    public long totalBytes() {
        return wm.getAllServeEvents().stream()
                .mapToLong(serveEvent -> serveEvent.getResponse().getBody().length)
                .sum();
    }

    @Override
    public <T, V> Optional<HttpExchange<T, V>> exchange(final HttpExchangeSearchParameters<T, V> parameters) {
        final List<HttpExchange<T, V>> found = exchanges(parameters);
        return found.isEmpty() ? Optional.empty() : Optional.of(found.get(0));
    }

    @Override
    public <T, V> List<HttpExchange<T, V>> exchanges(final HttpExchangeSearchParameters<T, V> parameters) {
        return new HttpExchangeSearcher<>(wm::getAllServeEvents, parameters).exchanges();
    }

    @Override
    public Response proxied() {
        return new ProxyResponse(fromBaseUrl, headers);
    }

    private String step(String scenario, int position) {
        return (position == 0) ? Scenario.STARTED : String.format("%s:%d", scenario, position);
    }

    private WireMockProxy proxy() {
        spawnWireMock();
        setUpWireMockProxy();
        return this;
    }

    private void spawnWireMock() {
        if (!wm.isRunning()) {
            wm.start();
            LOG.info("Started WireMock on port {}", wm.port());
        }
    }

    private void setUpWireMockProxy() {
        wm.stubFor(any(anyUrl()).willReturn(proxied().definition()).atPriority(Integer.MIN_VALUE));
    }
}
