package qa.bartszulc.proxy.request.agile.scrum;

import com.github.tomakehurst.wiremock.client.MappingBuilder;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.put;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import com.github.tomakehurst.wiremock.matching.MatchesJsonPathPattern;
import java.util.Optional;
import qa.bartszulc.proxy.request.Request;

public class IssueRankRequest implements Request {

    private final Integer rapidBoardId;

    public IssueRankRequest(int rapidBoardId) {
        this.rapidBoardId = rapidBoardId;
    }

    public IssueRankRequest() {
        this.rapidBoardId = null;
    }

    @Override
    public MappingBuilder mapping() {
        MappingBuilder builder = put(urlEqualTo("/rest/greenhopper/1.0/sprint/rank"));
        return Optional.ofNullable(rapidBoardId)
                .map(rapidBoardId -> builder.withRequestBody(new MatchesJsonPathPattern("$.rapidViewId", equalTo(rapidBoardId.toString()))))
                .orElse(builder);
    }

    @Override
    public String getName() {
        return IssueRankRequest.class.getSimpleName();
    }
}
