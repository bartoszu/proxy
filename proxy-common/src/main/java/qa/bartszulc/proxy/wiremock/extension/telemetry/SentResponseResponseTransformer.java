package qa.bartszulc.proxy.wiremock.extension.telemetry;

import com.github.tomakehurst.wiremock.common.FileSource;
import com.github.tomakehurst.wiremock.extension.Parameters;
import com.github.tomakehurst.wiremock.extension.ResponseTransformer;
import com.github.tomakehurst.wiremock.http.HttpHeader;
import com.github.tomakehurst.wiremock.http.HttpHeaders;
import com.github.tomakehurst.wiremock.http.Request;
import com.github.tomakehurst.wiremock.http.Response;
import java.util.Optional;

public class SentResponseResponseTransformer extends ResponseTransformer {
    public static final String SENT_RESPONSE_HEADER = "X-Proxy-SentResponse";

    @Override
    public Response transform(Request request, Response response, FileSource files, Parameters parameters) {
        final Response.Builder builder = Response.Builder.like(response);
        final HttpHeaders headers = Optional.ofNullable(response.getHeaders()).orElse(HttpHeaders.noHeaders());
        builder.headers(headers.plus(new HttpHeader(SENT_RESPONSE_HEADER, String.valueOf(System.currentTimeMillis()))));
        return builder.build();
    }

    @Override
    public String getName() {
        return SentResponseResponseTransformer.class.getName();
    }
}
