package qa.bartszulc.proxy.response;

import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import static qa.bartszulc.proxy.wiremock.extension.DeferredResponseDefinitionTransformer.DEFERRED_RESPONSE_HEADER;

public class DeferredResponse implements Response {
    private final int deferredMs;

    public DeferredResponse(int deferredMs) {
        this.deferredMs = deferredMs;
    }

    @Override
    public ResponseDefinitionBuilder append(ResponseDefinitionBuilder responseDefinitionBuilder) {
        return responseDefinitionBuilder.withHeader(DEFERRED_RESPONSE_HEADER, String.valueOf(deferredMs));
    }

    public static DeferredResponse deferred(int deferredMs) {
        return new DeferredResponse(deferredMs);
    }
}
