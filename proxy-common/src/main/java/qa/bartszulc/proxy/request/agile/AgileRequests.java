package qa.bartszulc.proxy.request.agile;

import qa.bartszulc.proxy.request.Request;
import qa.bartszulc.proxy.request.agile.scrum.ActiveSprintBoardDataRequest;
import qa.bartszulc.proxy.request.agile.scrum.BacklogDataRequest;
import qa.bartszulc.proxy.request.agile.scrum.CreateSprintRequest;
import qa.bartszulc.proxy.request.agile.scrum.DeleteSprintRequest;
import qa.bartszulc.proxy.request.agile.scrum.IssueRankRequest;
import qa.bartszulc.proxy.request.agile.scrum.MoveIssueBetweenColumnsRequest;
import qa.bartszulc.proxy.request.telemetry.BrowserMetricsRequest;

public final class AgileRequests {
    public static Request boardData() {
        return new ActiveSprintBoardDataRequest();
    }

    public static Request backlogData() {
        return new BacklogDataRequest();
    }

    public static Request moveIssueBetweenColumns() {
        return new MoveIssueBetweenColumnsRequest();
    }

    public static Request createSprint(int boardId) {
        return new CreateSprintRequest(boardId);
    }

    public static Request deleteSprint(int boardId) {
        return new DeleteSprintRequest(boardId);
    }

    public static Request issueRank(int boardId) {
        return new IssueRankRequest(boardId);
    }

    public static Request createSprint() {
        return new CreateSprintRequest();
    }

    public static Request deleteSprint() {
        return new DeleteSprintRequest();
    }

    public static Request issueRank() {
        return new IssueRankRequest();
    }

    public static final class Metrics {
        public static Request board() {
            return new BrowserMetricsRequest("jira.agile.work");
        }

        public static Request backlog() {
            return new BrowserMetricsRequest("jira.agile.plan");
        }
    }
}
