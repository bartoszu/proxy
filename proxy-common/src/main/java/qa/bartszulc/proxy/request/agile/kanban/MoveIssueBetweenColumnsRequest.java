package qa.bartszulc.proxy.request.agile.kanban;

import com.github.tomakehurst.wiremock.client.MappingBuilder;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.put;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;
import qa.bartszulc.proxy.request.Request;

public class MoveIssueBetweenColumnsRequest implements Request {
    @Override
    public MappingBuilder mapping() {
        return get(urlPathEqualTo("/secure/WorkflowUIDispatcher.jspa"));
    }

    @Override
    public String getName() {
        return MoveIssueBetweenColumnsRequest.class.getSimpleName();
    }
}
