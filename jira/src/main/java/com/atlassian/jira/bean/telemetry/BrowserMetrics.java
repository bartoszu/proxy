package com.atlassian.jira.bean.telemetry;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BrowserMetrics {

    private final String apdex;
    private final String firstPaint;
    private final String key;
    private final String readyForUser;

    @JsonCreator
    public BrowserMetrics(@JsonProperty("apdex") String apdex,
                          @JsonProperty("firstPaint") String firstPaint,
                          @JsonProperty("key") String key,
                          @JsonProperty("readyForUser") String readyForUser) {
        this.apdex = apdex;
        this.firstPaint = firstPaint;
        this.key = key;
        this.readyForUser = readyForUser;
    }

    public String getApdex() {
        return apdex;
    }

    public String getFirstPaint() {
        return firstPaint;
    }

    public String getKey() {
        return key;
    }

    public String getReadyForUser() {
        return readyForUser;
    }
}
