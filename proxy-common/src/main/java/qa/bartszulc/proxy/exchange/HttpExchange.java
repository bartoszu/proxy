package qa.bartszulc.proxy.exchange;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.http.HttpHeader;
import com.github.tomakehurst.wiremock.http.HttpHeaders;
import com.github.tomakehurst.wiremock.http.LoggedResponse;
import com.github.tomakehurst.wiremock.stubbing.ServeEvent;
import com.github.tomakehurst.wiremock.verification.LoggedRequest;
import com.jayway.jsonpath.JsonPath;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.Optional;
import java.util.zip.GZIPInputStream;
import net.minidev.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import qa.bartszulc.proxy.request.Request;
import qa.bartszulc.proxy.wiremock.extension.telemetry.ReceivedRequestResponseDefinitionTransformer;
import qa.bartszulc.proxy.wiremock.extension.telemetry.SentResponseResponseTransformer;

public final class HttpExchange<T, V> {
    private final Request origin;
    private final HttpEntity<T> request;
    private final HttpEntity<V> response;
    private final Optional<Long> requestReceived;
    private final Optional<Long> responseSent;

    public HttpExchange(final Request origin,
                        final HttpEntity<T> request,
                        final HttpEntity<V> response,
                        final Long requestReceived,
                        final Long responseSent) {
        this.origin = origin;
        this.request = request;
        this.response = response;
        this.requestReceived = Optional.ofNullable(requestReceived);
        this.responseSent = Optional.ofNullable(responseSent);
    }

    public static <T, V> HttpExchange<T, V> of(final ServeEvent event,
                                               final HttpExchangeSearchParameters<T, V> parameters) {
        final LoggedRequest request = event.getRequest();
        final HttpEntity<T> requestEntity = parameters.getRequestClass()
                .map(requestClass -> HttpEntity.of(request.getHeaders(), request.getBody(), request.getBodyAsString(), requestClass, parameters.getRequestJsonPath().orElse("$")))
                .orElse(HttpEntity.of(request.getHeaders()));

        final LoggedResponse response = event.getResponse();
        final HttpEntity<V> responseEntity = parameters.getResponseClass()
                .map(responseClass -> HttpEntity.of(response.getHeaders(), response.getBody(), response.getBodyAsString(), responseClass, parameters.getResponseJsonPath().orElse("$")))
                .orElse(HttpEntity.of(response.getHeaders()));

        return new HttpExchange<>(parameters.getRequest(), requestEntity, responseEntity,
                getHeaderValue(response.getHeaders(), ReceivedRequestResponseDefinitionTransformer.RECEIVED_REQUEST_HEADER),
                getHeaderValue(response.getHeaders(), SentResponseResponseTransformer.SENT_RESPONSE_HEADER));
    }

    private static Long getHeaderValue(final HttpHeaders headers, final String key) {
        final HttpHeader receivedRequestHeader = headers.getHeader(key);
        if (receivedRequestHeader.isPresent()) {
            return Long.valueOf(receivedRequestHeader.firstValue());
        }
        return null;
    }

    public Request getOrigin() {
        return origin;
    }

    public HttpEntity<T> getRequest() {
        return request;
    }

    public HttpEntity<V> getResponse() {
        return response;
    }

    public Optional<Long> getRequestReceived() {
        return requestReceived;
    }

    public Optional<Long> getResponseSent() {
        return responseSent;
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static final class HttpEntity<T> {
        private final HttpHeaders headers;
        private final Optional<T> entity;

        public HttpEntity(HttpHeaders headers, T entity) {
            this.headers = headers;
            this.entity = Optional.ofNullable(entity);
        }

        public static <T> HttpEntity<T> of(final HttpHeaders httpHeaders,
                                           final byte[] bytes,
                                           final String string,
                                           final Class<T> clazz,
                                           final String jsonPath) {
            final Factory<T> factory = new Factory<>();
            final T entity = factory.build(httpHeaders, bytes, string, clazz, jsonPath);
            return new HttpEntity<>(httpHeaders, entity);
        }

        public static <T> HttpEntity<T> of(final HttpHeaders httpHeaders) {
            return new HttpEntity<>(httpHeaders, null);
        }

        public HttpHeaders getHeaders() {
            return headers;
        }

        public Optional<T> getEntity() {
            return entity;
        }

        static class Factory<T> {
            private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
            private static final Logger LOG = LoggerFactory.getLogger(Factory.class);

            private T build(final HttpHeaders httpHeaders,
                            final byte[] bytes,
                            final String string,
                            final Class<T> clazz,
                            final String jsonPath) {
                try {
                    final HttpHeader encoding = httpHeaders.getHeader("Content-Encoding");
                    final String content = encoding.containsValue("gzip") ? gzipJson(bytes) : string;
                    final Object json = JsonPath.parse(content).read(jsonPath);

                    final Map<String, Object> map;
                    if (json instanceof JSONArray) {
                        map = (Map<String, Object>) ((JSONArray) json).get(0);
                    } else {
                        map = (Map<String, Object>) json;
                    }

                    return OBJECT_MAPPER.convertValue(map, clazz);
                } catch (Exception e) {
                    LOG.warn("couldn't deserialize response", e);
                    return null;
                }
            }

            private String gzipJson(byte[] bytes) throws IOException {
                final GZIPInputStream gzip = new GZIPInputStream(new ByteArrayInputStream(bytes));
                final InputStreamReader reader = new InputStreamReader(gzip);
                final BufferedReader in = new BufferedReader(reader);
                final StringBuilder json = new StringBuilder();
                String read;
                while ((read = in.readLine()) != null) {
                    json.append(read);
                }
                return json.toString();
            }
        }

    }
}
