package qa.bartszulc.proxy.response;

import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import com.github.tomakehurst.wiremock.extension.Extension;
import java.util.Map;
import wiremock.com.google.common.collect.ImmutableMap;

public class TransformedResponse implements Response {

    private final Extension extension;
    private final Map<String, Object> properties;

    public TransformedResponse(Extension extension, Map<String, Object> properties) {
        this.extension = extension;
        this.properties = properties;
    }

    public TransformedResponse(Extension extension) {
        this(extension, ImmutableMap.of());
    }

    public static TransformedResponse of(Extension extension) {
        return new TransformedResponse(extension);
    }

    public static TransformedResponse of(Extension extension, Map<String, Object> properties) {
        return new TransformedResponse(extension, properties);
    }

    @Override
    public ResponseDefinitionBuilder append(ResponseDefinitionBuilder responseDefinitionBuilder) {
        ResponseDefinitionBuilder builder = responseDefinitionBuilder.withTransformers(extension.getName());
        properties.forEach(builder::withTransformerParameter);
        return builder;
    }
}
