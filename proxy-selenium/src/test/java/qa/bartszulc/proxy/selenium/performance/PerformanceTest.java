package qa.bartszulc.proxy.selenium.performance;

import com.atlassian.jira.bean.telemetry.BrowserMetrics;
import static com.codeborne.selenide.Selenide.close;
import static com.codeborne.selenide.Selenide.executeJavaScript;
import com.github.tomakehurst.wiremock.extension.Extension;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import javax.annotation.Nullable;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runners.Parameterized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static qa.bartszulc.proxy.Constants.FIXTURES;
import qa.bartszulc.proxy.exchange.HttpExchange;
import static qa.bartszulc.proxy.exchange.HttpExchangeSearchParameters.request;
import qa.bartszulc.proxy.request.Request;
import qa.bartszulc.proxy.selenium.SeleniumTest;
import static qa.bartszulc.proxy.selenium.WireMockProxySupport.tryExtendedAuthProxy;
import qa.bartszulc.proxy.selenium.telemetry.Marker;
import qa.bartszulc.proxy.selenium.telemetry.Performance;
import qa.bartszulc.proxy.selenium.telemetry.Timing;
import qa.bartszulc.proxy.selenium.telemetry.result.PerformanceReport;
import qa.bartszulc.proxy.selenium.telemetry.result.PerformanceResult;
import qa.bartszulc.proxy.wiremock.WireMockProxy;
import qa.bartszulc.proxy.wiremock.extension.GeneratedIssuesResponseTransformer;

public abstract class PerformanceTest extends SeleniumTest {

    protected static final Extension ISSUES_EXT = new GeneratedIssuesResponseTransformer();

    private static final int ITERATIONS = 3;
    private static final int INITIAL_ISSUES = 10;

    private static final List<PerformanceResult> PERFORMANCE_RESULTS = Lists.newArrayListWithCapacity(ITERATIONS);
    private static final Logger LOG = LoggerFactory.getLogger(PerformanceTest.class);

    protected static WireMockProxy proxy;
    protected final int issues;

    public PerformanceTest(int issues) {
        this.issues = issues;
    }

    @BeforeClass
    public static void setupClass() {
        proxy = tryExtendedAuthProxy(PROXY_PORT, FIXTURES, JIRA_ADDRESS, JIRA_USERNAME, JIRA_PASSWORD, ISSUES_EXT);
        SeleniumTest.setupClass();
    }

    @AfterClass
    public static void teardownClass() {
        if (proxy == null) {
            return;
        }
        proxy.stop();
        LOG.info(new PerformanceReport(PERFORMANCE_RESULTS).render());
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return IntStream.range(0, ITERATIONS)
                .mapToObj(iteration -> new Object[]{issuesInIteration(iteration)})
                .collect(Collectors.toList());
    }

    private static int issuesInIteration(int iteration) {
        return (int) (INITIAL_ISSUES * Math.pow(10.0, iteration));
    }

    @Before
    public void setup() {
        proxy.reset();
    }

    @After
    public void teardown() {
        close();
    }

    protected <T, V> void recordPerformance(final HttpExchange<T, V> exchange, final Request browserMetricsRequest) {
        final Optional<HttpExchange<BrowserMetrics, Void>> maybeMetrics = proxy.exchange(request(browserMetricsRequest, BrowserMetrics.class));
        final Optional<Performance> maybePerformance = standardPerformance();
        maybePerformance.ifPresent(performance -> PERFORMANCE_RESULTS.add(performanceResults(exchange, performance, maybeMetrics.orElse(null))));
    }

    private <T, V> PerformanceResult performanceResults(
            final HttpExchange<T, V> exchange,
            final Performance performance,
            @Nullable HttpExchange<BrowserMetrics, Void> browserMetrics) {
        final Timing timing = performance.getTiming();
        return PerformanceResult.of(
                issues,
                timing.getNavigationStart(),
                (ImmutableList.of(
                        timing.getTotalNetworkTime(),
                        timing.getDomParsedTime(),
                        timing.getDomContentLoadedEventTime(),
                        timing.getDomRenderTime(),
                        timing.getDomTotalRenderTime(),
                        timing.getLoadEventTime(),
                        timing.getClientProcessingTime(),
                        timing.getTotalDuration())),
                browserMetrics,
                ImmutableList.of(exchange),
                ImmutableList.of(Marker.of("totalRequests", proxy.totalRequests()), Marker.of("totalBytes", proxy.totalBytes())));
    }

    private Optional<Performance> standardPerformance() {
        return Performance.of(executeJavaScript("return JSON.stringify(window.performance.toJSON())").toString());
    }
}