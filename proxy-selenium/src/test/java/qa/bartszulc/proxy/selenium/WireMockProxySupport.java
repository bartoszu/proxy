package qa.bartszulc.proxy.selenium;

import com.atlassian.jira.JiraClient;
import com.github.tomakehurst.wiremock.extension.Extension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import qa.bartszulc.proxy.wiremock.WireMockProxy;

public class WireMockProxySupport {
    private static final Logger LOG = LoggerFactory.getLogger(WireMockProxySupport.class);

    public static WireMockProxy proxy(int port, String hostname) {
        return new WireMockProxy(port).from(hostname);
    }

    public static WireMockProxy tryExtendedAuthProxy(int port, String directory,
                                                     String hostname, String username, String password,
                                                     Extension... extensions) {
        try {
            return extendedAuthProxy(port, directory, hostname, username, password, extensions);
        } catch (Exception e) {
            LOG.warn("couldn't create extendedAuthProxyProxy, trying authProxy", e);
            return tryAuthProxy(port, hostname, username, password);
        }
    }

    public static WireMockProxy tryAuthProxy(int port, String hostname, String username, String password) {
        try {
            return authProxy(port, hostname, username, password);
        } catch (Exception e) {
            LOG.warn("couldn't create authProxy, trying proxy", e);
            return proxy(port, hostname);
        }
    }

    public static WireMockProxy extendedAuthProxy(int port, String directory,
                                                  String hostname, String username, String password,
                                                  Extension... extensions) throws Exception {
        return new WireMockProxy(port, directory, extensions).from(hostname, sessionHeader(hostname, username, password));
    }

    public static WireMockProxy authProxy(int port, String hostname, String username, String password) throws Exception {
        return new WireMockProxy(port).from(hostname, sessionHeader(hostname, username, password));
    }

    private static WireMockProxy.Header sessionHeader(String hostname, String username, String password) throws Exception {
        final JiraClient jiraClient = new JiraClient(hostname, username, password);
        return jiraClient.getSession()
                .map(session -> new WireMockProxy.Header("Cookie", String.format("%s=%s; Path=/", session.getName(), session.getValue())))
                .orElseThrow(Exception::new);
    }
}
