package qa.bartszulc.proxy.request.agile.scrum;

import com.github.tomakehurst.wiremock.client.MappingBuilder;
import static com.github.tomakehurst.wiremock.client.WireMock.delete;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathMatching;
import java.util.Optional;
import qa.bartszulc.proxy.request.Request;

public class DeleteSprintRequest implements Request {

    private final Integer rapidBoardId;

    public DeleteSprintRequest(int rapidBoardId) {
        this.rapidBoardId = rapidBoardId;
    }

    public DeleteSprintRequest() {
        this.rapidBoardId = null;
    }

    public static DeleteSprintRequest of(int rapidBoardId) {
        return new DeleteSprintRequest(rapidBoardId);
    }

    @Override
    public MappingBuilder mapping() {
        MappingBuilder builder = delete(urlPathMatching("/rest/greenhopper/1.0/sprint/\\d+"));
        return Optional.ofNullable(rapidBoardId)
                .map(rapidBoardId -> builder.withQueryParam("rapidViewId", equalTo(rapidBoardId.toString())))
                .orElse(builder);
    }

    @Override
    public String getName() {
        return DeleteSprintRequest.class.getSimpleName();
    }
}
