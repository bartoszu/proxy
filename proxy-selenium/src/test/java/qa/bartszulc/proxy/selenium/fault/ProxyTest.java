package qa.bartszulc.proxy.selenium.fault;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import qa.bartszulc.proxy.selenium.SeleniumTest;
import qa.bartszulc.proxy.selenium.WireMockProxySupport;
import qa.bartszulc.proxy.wiremock.WireMockProxy;

public abstract class ProxyTest extends SeleniumTest {

    protected static WireMockProxy proxy;

    @BeforeClass
    public static void setupClass() {
        proxy = WireMockProxySupport.proxy(PROXY_PORT, JIRA_ADDRESS);
        SeleniumTest.setupClass();
    }

    @AfterClass
    public static void teardownClass() {
        proxy.stop();
    }

    @Before
    public void setup() {
        proxy.reset();
    }
}