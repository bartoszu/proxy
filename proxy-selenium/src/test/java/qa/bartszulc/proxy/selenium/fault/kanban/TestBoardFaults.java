package qa.bartszulc.proxy.selenium.fault.kanban;

import com.codeborne.selenide.Selenide;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.Before;
import org.junit.Test;
import qa.bartszulc.proxy.request.agile.kanban.ReleaseRequest;
import static qa.bartszulc.proxy.response.StatusResponse.forbidden;
import qa.bartszulc.proxy.selenium.fault.ProxyTest;
import qa.bartszulc.proxy.selenium.page.LoginPage;
import qa.bartszulc.proxy.selenium.page.Pages;
import qa.bartszulc.proxy.selenium.page.kanban.BoardPage;

public class TestBoardFaults extends ProxyTest {
    private static final String projectKey = "KANBAN001";
    private static final int boardId = 2;

    @Before
    public void setup() {
        Selenide.open("/login.jsp", LoginPage.class).login(JIRA_USERNAME, JIRA_PASSWORD);
    }

    @Test
    public void shouldDisplayErrorMessageOnReleaseForbidden() {
        // TODO: forbid releasing
    }
}
