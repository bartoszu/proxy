package qa.bartszulc.proxy.request.agile.scrum;

import com.github.tomakehurst.wiremock.client.MappingBuilder;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;
import qa.bartszulc.proxy.request.Request;

public class ActiveSprintBoardDataRequest implements Request {
    @Override
    public MappingBuilder mapping() {
        return get(urlPathEqualTo("/rest/greenhopper/1.0/xboard/work/allData.json"));
    }

    @Override
    public String getName() {
        return ActiveSprintBoardDataRequest.class.getSimpleName();
    }
}