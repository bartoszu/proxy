package qa.bartszulc.proxy.response;

import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;

public class DelayedResponse implements Response {

    private final int minDelayMs;
    private final int maxDelayMs;

    public DelayedResponse(int delayMs) {
        this(delayMs, delayMs);
    }

    public DelayedResponse(int minDelayMs, int maxDelayMs) {
        this.minDelayMs = minDelayMs;
        this.maxDelayMs = maxDelayMs;
    }

    public static DelayedResponse delayed(int delayMs) {
        return new DelayedResponse(delayMs);
    }

    @Override
    public ResponseDefinitionBuilder append(ResponseDefinitionBuilder responseDefinitionBuilder) {
        return responseDefinitionBuilder.withUniformRandomDelay(minDelayMs, maxDelayMs);
    }
}
