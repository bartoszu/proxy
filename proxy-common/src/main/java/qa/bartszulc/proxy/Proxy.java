package qa.bartszulc.proxy;

import java.util.List;
import java.util.Optional;
import qa.bartszulc.proxy.exchange.HttpExchange;
import qa.bartszulc.proxy.exchange.HttpExchangeSearchParameters;
import qa.bartszulc.proxy.request.Request;
import qa.bartszulc.proxy.response.Response;

public interface Proxy {

    Proxy from(String fromBaseUrl, Header... headers);

    Proxy always(Request request, Response response);

    Proxy stub(Request request, Response... response);

    Proxy once(Request request, Response response);

    Proxy reset();

    Response proxied();

    void stop();

    int totalRequests();

    long totalBytes();

    <T, V> Optional<HttpExchange<T, V>> exchange(final HttpExchangeSearchParameters<T, V> parameters);

    <T, V> List<HttpExchange<T, V>> exchanges(final HttpExchangeSearchParameters<T, V> parameters);

    final class Header {
        final String key;
        final String value;

        public Header(String key, String value) {
            this.key = key;
            this.value = value;
        }

        public String getKey() {
            return key;
        }

        public String getValue() {
            return value;
        }
    }
}
