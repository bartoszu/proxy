package qa.bartszulc.proxy.response;

import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import com.google.common.collect.ImmutableList;
import java.util.List;
import qa.bartszulc.proxy.Proxy;

public class ProxyResponse implements Response {

    private final String fromBaseUrl;
    private final List<Proxy.Header> headers;

    public ProxyResponse(final String fromBaseUrl, List<Proxy.Header> headers) {
        this.fromBaseUrl = fromBaseUrl;
        this.headers = ImmutableList.copyOf(headers);
    }

    @Override
    public ResponseDefinitionBuilder append(ResponseDefinitionBuilder responseDefinitionBuilder) {
        final ResponseDefinitionBuilder.ProxyResponseDefinitionBuilder responseBuilder = responseDefinitionBuilder.proxiedFrom(fromBaseUrl);
        headers.forEach(header -> responseBuilder.withAdditionalRequestHeader(header.getKey(), header.getValue()));
        responseBuilder.withHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        responseBuilder.withHeader("Pragma", "no-cache");
        responseBuilder.withHeader("Expires", "0");
        return responseBuilder;
    }
}
