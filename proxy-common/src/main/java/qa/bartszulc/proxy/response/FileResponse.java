package qa.bartszulc.proxy.response;

import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import org.apache.http.HttpHeaders;

public class FileResponse implements Response {

    private final String filename;
    private final String contentType;

    public FileResponse(String filename, String contentType) {
        this.filename = filename;
        this.contentType = contentType;
    }

    public FileResponse(String filename) {
        this(filename, "application/json");
    }

    public static FileResponse of(String filename) {
        return new FileResponse(filename);
    }

    @Override
    public ResponseDefinitionBuilder append(ResponseDefinitionBuilder responseDefinitionBuilder) {
        return responseDefinitionBuilder.withBodyFile(filename)
                .withHeader(HttpHeaders.CONTENT_TYPE, contentType);
    }
}
