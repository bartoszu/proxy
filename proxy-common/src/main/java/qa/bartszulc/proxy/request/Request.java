package qa.bartszulc.proxy.request;

import com.github.tomakehurst.wiremock.client.MappingBuilder;

public interface Request {

    MappingBuilder mapping();

    default String jsonPath() {
        return "$";
    }

    String getName();
}
