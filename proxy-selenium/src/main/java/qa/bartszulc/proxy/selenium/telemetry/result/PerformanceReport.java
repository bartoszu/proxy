package qa.bartszulc.proxy.selenium.telemetry.result;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import qa.bartszulc.proxy.selenium.telemetry.Marker;

public class PerformanceReport {
    private final List<PerformanceResult> performanceResults;

    public PerformanceReport(List<PerformanceResult> performanceResults) {
        this.performanceResults = ImmutableList.copyOf(performanceResults);
    }

    private String header(List<PerformanceResult> performanceResults) {
        final PerformanceResult first = performanceResults.get(0);
        return Stream.concat(Stream.of("issues"), first.getMarkers()
                .stream()
                .map(Marker::getKey))
                .collect(Collectors.joining(","));
    }

    private String row(PerformanceResult performanceResult) {
        return Stream.concat(Stream.of(String.valueOf(
                performanceResult.getIssues())),
                performanceResult.getMarkers()
                        .stream()
                        .map(Marker::getValue)
                        .map(String::valueOf))
                .collect(Collectors.joining(","));
    }

    private String rows(List<PerformanceResult> performanceResults) {
        return performanceResults.stream()
                .map(this::row)
                .collect(Collectors.joining("\n"));
    }

    private String table() {
        return String.join("\n", ImmutableList.of(header(performanceResults), rows(performanceResults)));
    }

    private String timelines() {
        return performanceResults.stream().map(this::timeline).collect(Collectors.joining("\n\n"));
    }

    private String timeline(PerformanceResult result) {
        final List<Marker<?>> markers = Lists.newArrayList(result.getMarkers());
        markers.sort(Marker::compare);
        return markers.stream()
                .map(marker -> marker.getEntry().map(entry -> String.format("%s-%s:%s",
                        entry,
                        marker.getExit().orElse(entry),
                        marker.getKey())).orElse(null))
                .filter(Objects::nonNull)
                .collect(Collectors.joining("\n"));
    }

    public String render() {
        if (!performanceResults.isEmpty()) {
            return String.join("\n\n", ImmutableList.of(table(), timelines()));
        }
        return "";
    }
}