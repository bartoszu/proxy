package qa.bartszulc.proxy.response;

import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;

public class StatusResponse implements Response {

    private final int status;

    public StatusResponse(int status) {
        this.status = status;
    }

    public static StatusResponse notfound() {
        return new StatusResponse(404);
    }

    public static StatusResponse unauthorized() {
        return new StatusResponse(401);
    }

    public static StatusResponse forbidden() {
        return new StatusResponse(403);
    }

    public static StatusResponse of(int status) {
        return new StatusResponse(status);
    }

    @Override
    public ResponseDefinitionBuilder append(ResponseDefinitionBuilder responseDefinitionBuilder) {
        return responseDefinitionBuilder.withStatus(status);
    }
}
