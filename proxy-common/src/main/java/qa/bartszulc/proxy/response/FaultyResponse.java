package qa.bartszulc.proxy.response;

import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import com.github.tomakehurst.wiremock.http.Fault;

public class FaultyResponse implements Response {
    private final Fault fault;

    public FaultyResponse(Fault fault) {
        this.fault = fault;
    }

    public static FaultyResponse of(Fault fault) {
        return new FaultyResponse(fault);
    }

    public static FaultyResponse malformed() {
        return FaultyResponse.of(Fault.MALFORMED_RESPONSE_CHUNK);
    }

    public static FaultyResponse connectionReset() {
        return FaultyResponse.of(Fault.CONNECTION_RESET_BY_PEER);
    }

    public static FaultyResponse emptyResponse() {
        return FaultyResponse.of(Fault.EMPTY_RESPONSE);
    }

    public static FaultyResponse randomData() {
        return FaultyResponse.of(Fault.RANDOM_DATA_THEN_CLOSE);
    }

    @Override
    public ResponseDefinitionBuilder append(ResponseDefinitionBuilder responseDefinitionBuilder) {
        return responseDefinitionBuilder.withFault(fault);
    }
}

