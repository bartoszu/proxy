package qa.bartszulc.proxy.request.agile.scrum;

import com.github.tomakehurst.wiremock.client.MappingBuilder;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathMatching;
import java.util.Optional;
import qa.bartszulc.proxy.request.Request;

public class CreateSprintRequest implements Request {

    private final Integer rapidBoardId;

    public CreateSprintRequest(int rapidBoardId) {
        this.rapidBoardId = rapidBoardId;
    }

    public CreateSprintRequest() {
        this.rapidBoardId = null;
    }

    public static CreateSprintRequest of(int rapidBoardId) {
        return new CreateSprintRequest(rapidBoardId);
    }

    @Override
    public MappingBuilder mapping() {
        return Optional.ofNullable(rapidBoardId)
                .map(id -> post(urlPathEqualTo(String.format("/rest/greenhopper/1.0/sprint/%s", id))))
                .orElse(post(urlPathMatching("/rest/greenhopper/1.0/sprint/\\d+")));
    }

    @Override
    public String getName() {
        return CreateSprintRequest.class.getSimpleName();
    }
}
