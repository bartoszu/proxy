package qa.bartszulc.proxy.selenium.telemetry;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Optional;


public class Timing {

    private final long navigationStart;
    private final long unloadEventStart;
    private final long unloadEventEnd;
    private final long redirectStart;
    private final long redirectEnd;
    private final long fetchStart;
    private final long domainLookupStart;
    private final long domainLookupEnd;
    private final long connectStart;
    private final long connectEnd;
    private final long secureConnectionStart;
    private final long requestStart;
    private final long responseStart;
    private final long responseEnd;
    private final long domLoading;
    private final long domInteractive;
    private final long domContentLoadedEventStart;
    private final long domContentLoadedEventEnd;
    private final long domComplete;
    private final long loadEventStart;
    private final long loadEventEnd;

    @JsonCreator
    public Timing(@JsonProperty("navigationStart") long navigationStart,
                  @JsonProperty("unloadEventStart") long unloadEventStart,
                  @JsonProperty("unloadEventEnd") long unloadEventEnd,
                  @JsonProperty("redirectStart") long redirectStart,
                  @JsonProperty("redirectEnd") long redirectEnd,
                  @JsonProperty("fetchStart") long fetchStart,
                  @JsonProperty("domainLookupStart") long domainLookupStart,
                  @JsonProperty("domainLookupEnd") long domainLookupEnd,
                  @JsonProperty("connectStart") long connectStart,
                  @JsonProperty("connectEnd") long connectEnd,
                  @JsonProperty("secureConnectionStart") long secureConnectionStart,
                  @JsonProperty("requestStart") long requestStart,
                  @JsonProperty("responseStart") long responseStart,
                  @JsonProperty("responseEnd") long responseEnd,
                  @JsonProperty("domLoading") long domLoading,
                  @JsonProperty("domInteractive") long domInteractive,
                  @JsonProperty("domContentLoadedEventStart") long domContentLoadedEventStart,
                  @JsonProperty("domContentLoadedEventEnd") long domContentLoadedEventEnd,
                  @JsonProperty("domComplete") long domComplete,
                  @JsonProperty("loadEventStart") long loadEventStart,
                  @JsonProperty("loadEventEnd") long loadEventEnd) {
        this.navigationStart = navigationStart;
        this.unloadEventStart = unloadEventStart;
        this.unloadEventEnd = unloadEventEnd;
        this.redirectStart = redirectStart;
        this.redirectEnd = redirectEnd;
        this.fetchStart = fetchStart;
        this.domainLookupStart = domainLookupStart;
        this.domainLookupEnd = domainLookupEnd;
        this.connectStart = connectStart;
        this.connectEnd = connectEnd;
        this.secureConnectionStart = secureConnectionStart;
        this.requestStart = requestStart;
        this.responseStart = responseStart;
        this.responseEnd = responseEnd;
        this.domLoading = domLoading;
        this.domInteractive = domInteractive;
        this.domContentLoadedEventStart = domContentLoadedEventStart;
        this.domContentLoadedEventEnd = domContentLoadedEventEnd;
        this.domComplete = domComplete;
        this.loadEventStart = loadEventStart;
        this.loadEventEnd = loadEventEnd;
    }

    public long getNavigationStart() {
        return navigationStart;
    }

    public long getUnloadEventStart() {
        return unloadEventStart;
    }

    public long getUnloadEventEnd() {
        return unloadEventEnd;
    }

    public long getRedirectStart() {
        return redirectStart;
    }

    public long getRedirectEnd() {
        return redirectEnd;
    }

    public long getFetchStart() {
        return fetchStart;
    }

    public long getDomainLookupStart() {
        return domainLookupStart;
    }

    public long getDomainLookupEnd() {
        return domainLookupEnd;
    }

    public long getConnectStart() {
        return connectStart;
    }

    public long getConnectEnd() {
        return connectEnd;
    }

    public long getSecureConnectionStart() {
        return secureConnectionStart;
    }

    public long getRequestStart() {
        return requestStart;
    }

    public long getResponseStart() {
        return responseStart;
    }

    public long getResponseEnd() {
        return responseEnd;
    }

    public long getDomLoading() {
        return domLoading;
    }

    public long getDomInteractive() {
        return domInteractive;
    }

    public long getDomContentLoadedEventStart() {
        return domContentLoadedEventStart;
    }

    public long getDomContentLoadedEventEnd() {
        return domContentLoadedEventEnd;
    }

    public long getDomComplete() {
        return domComplete;
    }

    public long getLoadEventStart() {
        return loadEventStart;
    }

    public long getLoadEventEnd() {
        return loadEventEnd;
    }

    public Optional<Marker<Long>> getDomContentLoadedEventTime() {
        return Marker.of(domContentLoadedEventStart - navigationStart, "domContentLoadedEventStart",
                domContentLoadedEventEnd - navigationStart, "domContentLoadedEventEnd",
                "domContentLoadedEventTime");
    }

    public Optional<Marker<Long>> getDomParsedTime() {
        return Marker.of(domLoading - navigationStart, "domLoading",
                domInteractive - navigationStart, "domInteractive"
                , "domParsedTime");
    }

    public Optional<Marker<Long>> getLoadEventTime() {
        return Marker.of(loadEventStart - navigationStart, "loadEventStart",
                loadEventEnd - navigationStart, "loadEventEnd",
                "loadEventTime");
    }

    public Optional<Marker<Long>> getDomTotalRenderTime() {
        return Marker.of(domLoading - navigationStart, "domLoading",
                domComplete - navigationStart, "domComplete",
                "domTotalRenderTime");
    }

    public Optional<Marker<Long>> getDomRenderTime() {
        return Marker.of(domContentLoadedEventEnd - navigationStart, "domContentLoadedEventEnd",
                domComplete - navigationStart, "domComplete",
                "domRenderTime");
    }

    public Optional<Marker<Long>> getTotalNetworkTime() {
        return Marker.of(0L, "navigationStart",
                responseEnd - navigationStart, "responseEnd",
                "totalNetworkTime");
    }

    public Optional<Marker<Long>> getClientProcessingTime() {
        return Marker.of(domLoading - navigationStart, "domLoading",
                loadEventEnd - navigationStart, "loadEventEnd",
                "clientProcessingTime");
    }

    public Optional<Marker<Long>> getTotalDuration() {
        return Marker.of(0L, "navigationStart",
                loadEventEnd - navigationStart, "loadEventEnd",
                "totalDuration");
    }
}
