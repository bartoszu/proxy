package com.atlassian.jira;

import com.atlassian.jira.bean.AuthRequest;
import com.atlassian.jira.bean.AuthResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.InputStreamReader;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wiremock.org.apache.http.client.methods.CloseableHttpResponse;
import wiremock.org.apache.http.client.methods.HttpPost;
import wiremock.org.apache.http.entity.StringEntity;
import wiremock.org.apache.http.impl.client.CloseableHttpClient;
import wiremock.org.apache.http.impl.client.HttpClients;
import wiremock.org.eclipse.jetty.http.HttpStatus;

public class JiraClient {
    private static final Logger LOG = LoggerFactory.getLogger(JiraClient.class);

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private final String hostname;
    private final String username;
    private final String password;
    private final CloseableHttpClient client;

    private AuthResponse.Session session = null;

    public JiraClient(String hostname, String username, String password) {
        this.hostname = hostname;
        this.username = username;
        this.password = password;
        this.client = HttpClients.createDefault();
    }

    public Optional<AuthResponse.Session> getSession() {
        if (session == null) {
            session = auth().map(AuthResponse::getSession).orElse(null);
        }
        return Optional.ofNullable(session);
    }

    private Optional<AuthResponse> auth() {
        try {
            final HttpPost httpPost = new HttpPost(String.format("%s/rest/auth/1/session", hostname));
            final AuthRequest authRequest = new AuthRequest(username, password);
            final String json = OBJECT_MAPPER.writeValueAsString(authRequest);
            final StringEntity entity = new StringEntity(json);
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            try (CloseableHttpResponse response = client.execute(httpPost)) {
                if (response.getStatusLine().getStatusCode() == HttpStatus.OK_200) {
                    AuthResponse authResponse = OBJECT_MAPPER.readValue(new InputStreamReader((response.getEntity().getContent())), AuthResponse.class);
                    return Optional.of(authResponse);
                }
            }
        } catch (Exception ex) {
            LOG.warn("Couldn't login to Jira", ex);
        }
        return Optional.empty();
    }
}
