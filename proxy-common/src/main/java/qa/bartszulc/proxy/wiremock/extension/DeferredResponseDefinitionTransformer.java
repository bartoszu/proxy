package qa.bartszulc.proxy.wiremock.extension;

import com.github.tomakehurst.wiremock.common.FileSource;
import com.github.tomakehurst.wiremock.extension.Parameters;
import com.github.tomakehurst.wiremock.extension.ResponseDefinitionTransformer;
import com.github.tomakehurst.wiremock.http.HttpHeaders;
import com.github.tomakehurst.wiremock.http.Request;
import com.github.tomakehurst.wiremock.http.ResponseDefinition;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeferredResponseDefinitionTransformer extends ResponseDefinitionTransformer {
    private static final Logger LOG = LoggerFactory.getLogger(DeferredResponseDefinitionTransformer.class);
    public static final String DEFERRED_RESPONSE_HEADER = "X-Proxy-Deferred";

    @Override
    public ResponseDefinition transform(Request request,
                                        ResponseDefinition responseDefinition,
                                        FileSource files,
                                        Parameters parameters) {
        final HttpHeaders headers = Optional.ofNullable(responseDefinition.getHeaders()).orElse(HttpHeaders.noHeaders());
        if (headers.keys().contains(DEFERRED_RESPONSE_HEADER)) {
            final long deferred = Long.parseLong(headers.getHeader(DEFERRED_RESPONSE_HEADER).firstValue());
            try {
                LOG.info("deferring response by {}ms", deferred);
                Thread.sleep(deferred);
            } catch (InterruptedException e) {
                LOG.warn("exception occurred while deferring response", e);
            }
        }
        return responseDefinition;
    }

    @Override
    public String getName() {
        return DeferredResponseDefinitionTransformer.class.getName();
    }
}
