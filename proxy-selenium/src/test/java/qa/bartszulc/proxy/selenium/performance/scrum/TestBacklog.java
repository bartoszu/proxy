package qa.bartszulc.proxy.selenium.performance.scrum;

import static com.codeborne.selenide.Selenide.open;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import static qa.bartszulc.proxy.exchange.HttpExchangeSearchParameters.request;
import qa.bartszulc.proxy.request.agile.AgileRequests;
import static qa.bartszulc.proxy.request.agile.AgileRequests.backlogData;
import qa.bartszulc.proxy.response.FileResponse;
import qa.bartszulc.proxy.response.TransformedResponse;
import static qa.bartszulc.proxy.selenium.page.Pages.backlogPage;
import qa.bartszulc.proxy.selenium.page.scrum.BacklogPage;
import qa.bartszulc.proxy.selenium.performance.PerformanceTest;
import static qa.bartszulc.proxy.wiremock.extension.GeneratedIssuesResponseTransformer.ISSUES_PROJECT_KEY;
import static qa.bartszulc.proxy.wiremock.extension.GeneratedIssuesResponseTransformer.NUM_ISSUES_KEY;

@RunWith(Parameterized.class)
public class TestBacklog extends PerformanceTest {
    private static final String PROJECT_KEY = "SCRUM001";
    private static final int BOARD_ID = 1;

    public TestBacklog(int issues) {
        super(issues);
    }

    @Test
    public void openBacklog() {
        // TODO
    }
}
