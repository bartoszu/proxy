package qa.bartszulc.proxy.selenium.telemetry;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.Optional;

public class Performance {
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private final float timeOrigin;
    private final Timing timing;
    private final Navigation navigation;

    @JsonCreator
    public Performance(@JsonProperty("timeOrigin") float timeOrigin,
                       @JsonProperty("timing") Timing timing,
                       @JsonProperty("navigation") Navigation navigation) {
        this.timeOrigin = timeOrigin;
        this.timing = timing;
        this.navigation = navigation;
    }

    public static Optional<Performance> of(String content) {
        try {
            return Optional.ofNullable(OBJECT_MAPPER.readValue(content, Performance.class));
        } catch (IOException e) {
            return Optional.empty();
        }
    }

    public float getTimeOrigin() {
        return timeOrigin;
    }

    public Timing getTiming() {
        return timing;
    }

    public Navigation getNavigation() {
        return navigation;
    }
}
